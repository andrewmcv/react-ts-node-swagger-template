# Skeleton project: simple client and server example, with a shared library

* All in Typescript
* Using Yarn workspaces + monorepo
* Client is React/Ant Design
* Server is Express/Swagger
* Shared workspace can be used by both client & server
* Code reloading / restarting supported for changes in any workspace - changes to shared will trigger both client & server reload

# Running it

First, clone the project:

```
git clone git@bitbucket.org:andrewmcv/react-ts-node-swagger-template.git
cd react-ts-node-swagger-template
```

Next, check you have the latest version of yarn - should ideally be 3.2+

```
yarn set version latest
yarn -v
```

Then install all the packages:
   
```
yarn install
```

Finally run both client & server:

```
yarn start
```

# Client Screen

It should start up a screen in the browser that looks like this:

<img src="docs/browser.png" alt="client" width="800"/>

# Server Documentation

Go here to see the server API and try it out: <a href="http://localhost:8000/docs/">Swagger UI</a>

<img src="docs/swagger.png" alt="server" width="800"/>






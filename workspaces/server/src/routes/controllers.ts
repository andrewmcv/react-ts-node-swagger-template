import { Body, Get, Path, Post, Query, Route, Tags } from "tsoa";
import * as Api from "@demo/shared/src/index"


@Route("ping")
export class PingController {
  /**
   * Demo: GET with path and query params
   */
  @Tags("pingpong")
  @Get("{id}")
  public async pong1(@Path() id: string, @Query() name?: string): Promise<Api.PingResponse> {
    return {
      outputMessage: "GET pong1: " + id + "/" + name
    };
  }

  /**
   * Demo: GET with just query params
   */
   @Tags("pingpong")
   @Get()
  public async pong2(@Query() name: string): Promise<Api.PingResponse> {
    return {
      outputMessage: "GET pong2: " + name 
    };
  }

  /**
   * Demo: POST with id, query params & body
   */
   @Tags("pingpong")
   @Post()
  public async pong3(@Query() name: string, @Body() body: Api.PingRequest): Promise<Api.PingResponse> {
    return {
      outputMessage: "POST pong3: " + name + " -- " + body.inputMessage
    };
  }
}
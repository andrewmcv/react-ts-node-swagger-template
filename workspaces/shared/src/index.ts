

export interface PingRequest {
    inputMessage: string
}

export interface PingResponse {
    outputMessage: string
}

import React from 'react';
import logo from './logo.svg';
import './App.css';
import * as Api from "@demo/shared/src/index"
import { Button } from 'antd'
import { Tabs } from 'antd'
const { TabPane } = Tabs;

interface IAppState {
  title: string
}

export class App extends React.Component<{}, IAppState> {
  state = { title: "before call to server" }

  async callApi<R>() {
    const message = "from client"
    const response = await fetch(`/ping/10?name=${message}`, {
      method: "get"
    })
    const body = await response.json()
    if (response.status !== 200) throw Error(body.message)
    return body as R
  }

  componentDidMount = () => {
    this.callApi<Api.PingResponse>().then(value => {
      this.setState({ title: "after call to server: " + value.outputMessage })
    })
  }

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <p>
            Edit <code>src/App.tsx</code> and save to reload.
          </p>
          <a
            className="App-link"
            href="https://reactjs.org"
            target="_blank"
            rel="noopener noreferrer"
          >
            React client with server + shared library: {this.state.title}
          </a>
          <Tabs defaultActiveKey="1">
            <TabPane tab="Tab 1" key="1">
              Content of Tab Pane 1
            </TabPane>
            <TabPane tab="Tab 2" key="2">
              Content of Tab Pane 2
            </TabPane>
            <TabPane tab="Tab 3" key="3">
              Content of Tab Pane 3
              <div className="App">
                <Button type="primary">Button</Button>
              </div>
            </TabPane>
          </Tabs>          
        </header>
      </div>
    );
  }
}

export default App;
